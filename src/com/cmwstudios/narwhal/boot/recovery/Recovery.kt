/*
 * Copyright (c) 2018. Aidan Veney under Creative Commons ShareAlike 3.0 (CC BY-SA 3.0 US)
 */

package com.cmwstudios.narwhal.boot.recovery

import com.cmwstudios.hiya.jash.PrivilageLevel
import com.cmwstudios.narwhal.Narwhal
import com.cmwstudios.narwhal.boot.BootMode
import com.cmwstudios.narwhal.boot.Bootloader
import com.cmwstudios.narwhal.exception.LigmaException
import com.cmwstudios.narwhal.lg.LetsGo

import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader

class Recovery(reason: RecoveryReason) {
    init {
        System.out.println("Project Narwhal Recovery")
        System.out.println("Recovery Reason: $reason")
        try {
            //            ShutdownJob shutdownJob = new ShutdownJob(true);
            prompt("lol", PrivilageLevel.USER)
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    companion object {

        fun rebootRecovery(reason: RecoveryReason) {
            //        try {
            //            ShutdownJob s = new ShutdownJob(false);
            //        } catch (InterruptedException e) {
            //            e.printStackTrace();
            //        }
            val b = Bootloader(BootMode.RECOVERY, reason)
        }

        @Throws(IOException::class)
        private fun prompt(dir: String, priv: PrivilageLevel) {
            if (Narwhal.stopping) {
                return
            }
            //        System.out.append(dir + " $ ");
            System.out.append("HELP > ")
            val br = BufferedReader(InputStreamReader(System.`in`))
            val s = Short.parseShort(br.readLine())
            if (s.toInt() == 0) {
                System.exit(48)
            } else if (s.toInt() == 1) {
                System.out.println("Restarting...")
                val b = Bootloader(BootMode.STANDARD, null)
                b.letsGo(LetsGo.PIKACHU)
            } else if (s.toInt() == 2) {
                try {
                    throw LigmaException()
                } catch (e: LigmaException) {
                    e.printStackTrace()
                }

            }
            //        String j = s.replace(dir + " $", "");
            //        try {
            //            JASHInterpreter jashInterpreter = new JASHInterpreter(s);
            //        } catch (InterruptedException e) {
            //            e.printStackTrace();
            //        }
            //        sys(j);
            //        sys("java -jar " + j);
            prompt(dir, priv)
        }
    }

}
