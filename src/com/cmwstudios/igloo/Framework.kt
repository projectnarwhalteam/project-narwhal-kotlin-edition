package com.cmwstudios.igloo

interface Framework {
    companion object {
        val softwareName = "Project Narwhal"
        val softwareVersion = "Alpha 2"
    }
}
