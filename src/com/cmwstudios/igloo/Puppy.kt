package com.cmwstudios.igloo

internal class Puppy(name: String, age: Int, color: Color) {
    init {
        System.out.println(name + " is " + age + "years old and is " + color)
    }
}
